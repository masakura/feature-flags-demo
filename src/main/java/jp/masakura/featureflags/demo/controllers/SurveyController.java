package jp.masakura.featureflags.demo.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public final class SurveyController {
    @RequestMapping("/survey")
    public String index() {
        return "survey/index";
    }
}
