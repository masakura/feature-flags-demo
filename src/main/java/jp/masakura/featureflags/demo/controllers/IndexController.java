package jp.masakura.featureflags.demo.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public final class IndexController {
    @RequestMapping("/")
    public String index() {
        return "index";
    }
}
