package jp.masakura.featureflags.demo.controllers;

import jp.masakura.featureflags.demo.config.Environments;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

@ControllerAdvice
public final class EnvironmentsAdvice {
    private final Environments environments;

    public EnvironmentsAdvice(Environments environments) {
        this.environments = environments;
    }

    @ModelAttribute
    public void model(Model model) {
        model.addAttribute("environments", this.environments);
    }
}
