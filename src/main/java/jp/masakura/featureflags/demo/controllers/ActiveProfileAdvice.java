package jp.masakura.featureflags.demo.controllers;

import jp.masakura.featureflags.demo.config.ActiveProfile;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

@ControllerAdvice
public final class ActiveProfileAdvice {
    private final ActiveProfile activeProfile;

    public ActiveProfileAdvice(ActiveProfile activeProfile) {
        this.activeProfile = activeProfile;
    }

    @ModelAttribute
    public void model(Model model) {
        model.addAttribute("activeProfile", activeProfile.getName());
    }
}
