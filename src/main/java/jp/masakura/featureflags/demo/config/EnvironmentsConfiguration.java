package jp.masakura.featureflags.demo.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class EnvironmentsConfiguration {
    @Bean
    public Environments environmentsConfig(@Value("${ENVIRONMENT_LIST:}") String environments) {
        return Environments.parse(environments);
    }
}
