package jp.masakura.featureflags.demo.config;

public final class Environment {
    private final String name;
    private final String url;

    public Environment(String name, String url) {
        this.name = name;
        this.url = url;
    }

    public static Environment parse(String node) {
        String[] parts = node.split("=");
        return new Environment(parts[0], parts[1]);
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }
}

