package jp.masakura.featureflags.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class ActiveProfileConfiguration {
    @Bean
    public ActiveProfile activeProfile(Environment environment) {
        return ActiveProfile.create(environment);
    }
}
