package jp.masakura.featureflags.demo.config;

import no.finn.unleash.DefaultUnleash;
import no.finn.unleash.Unleash;
import no.finn.unleash.util.UnleashConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UnleashConfiguration {
    @Bean
    public UnleashConfig unleashConfig(ActiveProfile activeProfile,
                                       @Value("${GITLAB_FEATURE_FLAGS_API_URL}") String apiUrl,
                                       @Value("${GITLAB_FEATURE_FLAGS_INSTANCE_ID}") String instanceId) {
        return UnleashConfig.builder()
                .appName(activeProfile.getName())
                .instanceId(instanceId)
                .unleashAPI(apiUrl)
                .build();
    }

    @Bean
    public Unleash unleash(UnleashConfig unleashConfig) {
        return new DefaultUnleash(unleashConfig);
    }
}
