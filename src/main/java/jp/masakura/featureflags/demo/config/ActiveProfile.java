package jp.masakura.featureflags.demo.config;

import org.springframework.core.env.Environment;

import java.util.Arrays;
import java.util.Optional;

public final class ActiveProfile {
    private final String name;

    private ActiveProfile(String profile) {
        this.name = profile;
    }

    public String getName() {
        return name;
    }

    public static ActiveProfile create(Environment environment) {
        return new ActiveProfile(activeProfile(environment));
    }

    private static String activeProfile(Environment environment) {
        return getName(environment)
                .orElse(getDefaultProfile(environment).orElseThrow());
    }

    private static Optional<String> getName(Environment environment) {
        return Arrays.stream(environment.getActiveProfiles()).findFirst();
    }

    private static Optional<String> getDefaultProfile(Environment environment) {
        return Arrays.stream(environment.getDefaultProfiles()).findFirst();
    }
}
