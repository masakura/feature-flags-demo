package jp.masakura.featureflags.demo.config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public final class Environments implements Iterable<Environment> {
    private final List<Environment> environments;

    public Environments(List<Environment> environments) {
        this.environments = environments;
    }

    public boolean isEmpty() {
        return this.environments.size() <= 0;
    }

    public static Environments parse(String values) {
        if (values.isEmpty()) return new Environments(new ArrayList<>());

        List<Environment> list = Arrays.stream(values.split(";"))
                .map(Environment::parse)
                .collect(Collectors.toList());
        return new Environments(list);
    }

    @Override
    public Iterator<Environment> iterator() {
        return environments.iterator();
    }
}
