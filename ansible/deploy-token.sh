#!/bin/bash

function value() {
  echo "$1" | grep "$2" | sed 's/.*:\s*"//; s/".*//;'
}

principal=$(az ad sp create-for-rbac --name feature-flags-gitlab)

AZURE_TENANT=$(value "$principal" tenant)
AZURE_AD_USER=$(value "$principal" appId)
AZURE_PASSWORD=$(value "$principal" password)

for host in $(ansible --list-hosts webapps | tail -n +2 | sed 's/^\s*//; s/\..*$//;'); do
  az role assignment create --subscription 17d4b359-6af6-412f-9bb3-2e802ab98c26 \
    --assignee d9f9d0e4-9bd5-4d23-8092-d217911026aa \
    --role Contributor \
    --scope "/subscriptions/17d4b359-6af6-412f-9bb3-2e802ab98c26/resourceGroups/gitlab-feature-flags/providers/Microsoft.Web/sites/$host"
done

echo AZURE_TENANT="$AZURE_TENANT"
echo AZURE_AD_USER="$AZURE_AD_USER"
echo AZURE_PASSWORD="$AZURE_PASSWORD"
